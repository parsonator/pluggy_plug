﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Cable : MonoBehaviour {

    SpringJoint cableJoint;
    public LineRenderer cable;
    public Transform[] points;

    public void setCable(Rigidbody _connect, float _distence )
    {
        cableJoint.connectedBody = _connect;
        cableJoint.maxDistance = _distence;

    }

    void Awake()
    {
        if (cableJoint == null)
        {
            if ((cableJoint = GetComponent<SpringJoint>())==null)
                cableJoint = gameObject.AddComponent<SpringJoint>();
        }
        
        cable = GetComponent<LineRenderer>();
        cable.positionCount = 3;

    }

	// Update is called once per frame
	void Update ()
    {
        cable.SetPosition(0, points[0].position);
        cable.SetPosition(1,Vector3.Lerp(points[0].position, points[1].position,0.5f) +Vector3.up );
        cable.SetPosition(2, points[1].position);
    }

    
}
