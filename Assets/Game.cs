﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Game : MonoBehaviour {

    public bool allDevicesOn;
    public static int levelNo = 1;

    bool loadingLevel;

    void Awake()
    {
        allDevicesOn = false;
        StopAllCoroutines();

    }

    // Use this for initialization
    void Start () {
        allDevicesOn = false;
    }

    // Update is called once per frame
    void Update () {
        allDevicesOn = CheckLevel() ? true : allDevicesOn;
        if(allDevicesOn && !loadingLevel)
        {
            StartCoroutine(loadNextScene());
        }
	}

    bool CheckLevel()
    {
        Device[] allDevice;
        allDevice = FindObjectsOfType<Device>();
        foreach (Device d in allDevice)
        {
            if(d.haspower == false)
            {
                return false;
            }
        }
        return true;

    }

    IEnumerator loadNextScene()
    {
        if (loadingLevel) yield break;
        print("wait");
        loadingLevel = true;
        allDevicesOn = false;
        yield return new WaitForSeconds(2);
        SceneManager.LoadSceneAsync(levelNo, LoadSceneMode.Single);
        print("loading level " + levelNo);
        levelNo++;
        allDevicesOn = false;
        loadingLevel = false;
        

    }
}
