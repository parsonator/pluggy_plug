﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Rigidbody selected;

    [SerializeField]
    float liftHight = 5;
    [SerializeField]
    float moveSpeed = 5;

    AudioSource audioSource;

    public AudioClip onClick;
    public AudioClip OffClick;

    // Update is called once per frame

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate () {
	    
        if(Input.GetButton("Fire1"))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Physics.Raycast(ray, out hit);

            if (selected == null && hit.transform.gameObject.layer == 8)
            {
                audioSource.PlayOneShot(onClick);
                selected = hit.transform.GetComponent<Rigidbody>();
                if (selected.GetComponent<Plug>() != null)
                {
                    selected.GetComponent<Plug>().selected = true;
                }
            }
            else if(selected != null)
            {
                Vector3 p = hit.point;
                p.y = 0;
                selected.MovePosition( p + Vector3.up * liftHight);
                selected.MoveRotation(Quaternion.LookRotation(Vector3.up));
            }
        }
        else
        {
            if (selected != null)
            {
                audioSource.PlayOneShot(OffClick);
                if (selected.GetComponent<Plug>() != null)
                {
                    selected.GetComponent<Plug>().selected = false;
                }
                selected = null;
            }
        }

	}
}
