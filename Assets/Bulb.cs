﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulb : Device {

    [SerializeField]
    Material bulb;
    [SerializeField]
    Color onColour,offColour;
    [SerializeField]
    Light bulbLight;
	
	// Update is called once per frame
	void Update () {
        haspower = devicePlug.hasPower;
        bulb.color = haspower ? onColour : offColour;
        bulbLight.enabled = haspower;
        
	}
}
