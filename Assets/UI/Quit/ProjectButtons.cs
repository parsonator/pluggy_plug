﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectButtons : MonoBehaviour {
	public void OpenWebPage(string URL)
	{
        Application.OpenURL(URL);
	}

	public void QuitGame(string URL)
	{
        Application.OpenURL(URL);
		Invoke("Application.Quit()",1);
	}

}
