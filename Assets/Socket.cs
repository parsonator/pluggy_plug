﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Socket : MonoBehaviour
{

    public Transform connectPoint;
    public Plug connectedPlug;
    public Plug_Type type;
    public bool connected;
    public bool hasPower;
    public Rigidbody rb;
    public AudioClip connectedSound;
    
    AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        
        rb = GetComponentInParent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (connectedPlug != null)
        {
            connectedPlug.hasPower = hasPower;

            if (connectedPlug.selected == true)
                disconnectPlug();

            return;
        }
        Plug p;
        RaycastHit[] hits = Physics.SphereCastAll(connectPoint.position, 0.9f, Vector3.up);
        foreach (RaycastHit h in hits)
        {
            if ((p = h.transform.GetComponent<Plug>()) != null && !p.selected )
            {
                h.transform.GetComponent<Plug>();
                connectPlug(p);
                break;
            }
        }

    }

   public void connectPlug(Plug _plug)
    {

        if (_plug.type == type)
        {
            print("connectPlug");
            audioSource.PlayOneShot(connectedSound);
            _plug.rb.isKinematic = true;
            _plug.transform.position =(connectPoint.transform.position);
            _plug.transform.rotation = (connectPoint.rotation);
            _plug.transform.parent = transform;
            connectedPlug = _plug;
        }
    }
    public void disconnectPlug()
    {
        print("disconnectPlug");
        connectedPlug.rb.isKinematic = false;
        connectedPlug.transform.parent = null;
        connectedPlug.hasPower = false;
        connectedPlug = null;
    }

}

public enum Plug_Type
{
    US_PLUG,
    UK_PLUG,
    OTHER_PLUG
}
