﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtCord : MonoBehaviour
{
    public float cableLeanth = 16;
    public Socket[] plugSockets;
    public Plug extPlug;
    public bool hasPower;
    public bool connected;
    public bool isPowerPoint;
    public Rigidbody rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {
        plugSockets = GetComponentsInChildren<Socket>();

        if (isPowerPoint)
        {
            foreach (Socket socket in plugSockets)
            {
                socket.hasPower = true;
            }
        }
        else
        {
            extPlug.cable.setCable(rb, cableLeanth);
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (isPowerPoint)
            return;
        hasPower = extPlug.hasPower;

        foreach (Socket Socket in plugSockets)
        {
            Socket.hasPower = hasPower;
        }


    }
}
