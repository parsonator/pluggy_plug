﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plug : MonoBehaviour
{
    public bool hasPower;
    public bool selected;
    public Plug_Type type;
    public Rigidbody rb;
    public Cable cable;

    void Awake()
    {
        cable = GetComponent<Cable>();
        rb = GetComponent<Rigidbody>();
    }

}
